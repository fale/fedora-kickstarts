# fedora-livecd-sway.ks
#
# Description:
# - Fedora Live Spin with the tiling window manager Sway
#
# Maintainer(s):
# - Aleksei Bavshin    <alebastr89@gmail.com>
# - Jiří Konečný       <jkonecny@redhat.com>
# - Anthony Rabbito    <hello@anthonyrabbito.com>

%include fedora-live-base.ks
%include fedora-live-minimization.ks
%include fedora-sway-common.ks

%post
# create /etc/sysconfig/desktop (needed for installation)
cat > /etc/sysconfig/desktop <<EOF
PREFERRED=/usr/bin/sway
DISPLAYMANAGER=/bin/sddm
EOF

# add initscript
cat >> /etc/rc.d/init.d/livesys << EOF

SWAY_SESSION_FILE="sway.desktop"

# set up autologin for user liveuser
if [ -f /etc/sddm.conf ]; then
sed -i 's/^#User=.*/User=liveuser/' /etc/sddm.conf
sed -i "s/^#Session=.*/Session=\${SWAY_SESSION_FILE}/" /etc/sddm.conf
else
cat > /etc/sddm.conf << SDDM_EOF
[Autologin]
User=liveuser
Session=\${SWAY_SESSION_FILE}
SDDM_EOF
fi

# Show harddisk install on the desktop
sed -i -e 's/NoDisplay=true/NoDisplay=false/' /usr/share/applications/liveinst.desktop
mkdir /home/liveuser/Desktop

# echoing type liveinst to start the installer
echo "echo 'Please type liveinst and press Enter to start the installer'" >> /home/liveuser/.bashrc

# fixing the installer non opening bug
# this should ideally go into the /home/liveuser/.profile but SDDM won't source that
echo "xhost si:localuser:root" >> /home/liveuser/.bashrc

# this goes at the end after all other changes.
chown -R liveuser:liveuser /home/liveuser
restorecon -R /home/liveuser

EOF

%end

