# fedora-livecd-sway.ks
#
# Description:
# - Fedora Live Spin with the tiling window manager i3wm
#
# Maintainer(s):
# - Aleksei Bavshin    <alebastr89@gmail.com>
# - Jiří Konečný       <jkonecny@redhat.com>
# - Anthony Rabbito    <hello@anthonyrabbito.com>


%packages
fedora-release-sway
@^sway-desktop-environment
@firefox
@sway-desktop
%end
